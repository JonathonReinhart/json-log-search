json-log-search
===============
Search JSON log files (optionally gzip-compressed) for keys/values matching certain filters

This tool almost certainly exists, but it seemed easier to write than to discover/learn/fix/etc.
