#!/usr/bin/env python3
import json
import gzip
import re

def smart_open(path, mode='rt'):
    gf = gzip.open(path, mode)
    try:
        gf.read(1)
    except OSError:
        pass
    else:
        gf.seek(0)
        return gf

    return open(path, mode)


class KeyFilter:
    def __init__(self, key, value_pattern):
        self.key = key
        self.valpat = re.compile('^' + value_pattern + '$')

    @classmethod
    def parse(cls, s):
        k, v = s.split('=', 1)
        return cls(k, v)

    def match(self, data):
        try:
            value = data[self.key]
        except KeyError:
            return False

        return self.valpat.match(value)


def process(f, filters):
    def all_filters_match(data):
        for filt in filters:
            if not filt.match(data):
                return False
        return True

    for line in f:
        e = json.loads(line)
        if not all_filters_match(e):
            continue
        print(json.dumps(e, sort_keys=True, indent=4))


def parse_args():
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('infile', nargs='+')
    ap.add_argument('-f', '--filter', dest='filters', action='append',
            type=KeyFilter.parse,
            help="Filters to apply (ANDed together)")
    return ap.parse_args()


def main():
    args = parse_args()
    for path in args.infile:
        with smart_open(path) as f:
            process(f, args.filters)

if __name__ == '__main__':
    main()
